<?php

namespace App\Helpers;

class PathsHelper
{
    public static function makeFolderIfDoNotExists($folder)
    {
        if (!file_exists($folder)) {
            mkdir($folder);
        }
        return $folder;
    }

    public static function uniqueFileName($pathToFile)
    {
        if (file_exists($pathToFile)) {
            $pathInfo = pathinfo($pathToFile);
            $totalFiles = count(glob($pathInfo['dirname'] . '/*'));
            return $pathInfo['filename'] . '_' . $totalFiles . '.' . $pathInfo['extension'];
        }
        return basename($pathToFile);
    }

    public static function getHallPhoto($photoName, $url = true)
    {
        $pathToHallPhoto = 'uploads/halls/' . $photoName;
        if ($url) {
            $pathToHallPhoto = asset($pathToHallPhoto);
        }
        return $pathToHallPhoto;
    }
}