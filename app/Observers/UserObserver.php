<?php

namespace App\Observers;

use App\Models\Reservation;
use App\User;

class UserObserver
{
    public function deleting(User $event)
    {
        Reservation::where('user_id', $event->id)->delete();
    }
}
