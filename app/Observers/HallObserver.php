<?php

namespace App\Observers;

use App\Helpers\PathsHelper;
use App\Models\Hall;
use App\Models\Reservation;

class HallObserver
{
    public function updated(Hall $event)
    {
        $valuesBefore = $event->getOriginal();
        $valuesAfter = $event->getAttributes();

        if ($valuesBefore['photo'] != $valuesAfter['photo'] && !empty($valuesBefore['photo'])) {
            @unlink(PathsHelper::getHallPhoto($valuesBefore['photo'], false));
        }
    }

    public function deleting(Hall $event)
    {
        @unlink(PathsHelper::getHallPhoto($event->photo, false));
        Reservation::where('hall_id', $event->id)->delete();
    }
}
