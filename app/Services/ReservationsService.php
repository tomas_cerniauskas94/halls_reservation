<?php

namespace App\Services;

use App\Models\Reservation;
use App\User;

class ReservationsService
{
    public function getEditableUserReservation(User $user, $eventId)
    {
        $reservation = Reservation::where('id', $eventId);
        if (!$user->is_admin) {
            $reservation = $reservation->where('user_id', $user->id);
        }
        $reservation = $reservation->first();
        return $reservation;
    }

    public function getReservations($params = [])
    {
        $reservations = new Reservation;
        if (!empty($params['user_id'])) {
            $reservations = $reservations->where('user_id', $params['user_id']);
        }
        if(!empty($params['hall_id'])){
            $reservations = $reservations->where('hall_id', $params['hall_id']);
        }
        if (!empty($params['order_by']) && !empty($params['order'])) {
            $reservations = $reservations->orderBy($params['orderBy'], $params['order']);
        } else {
            $reservations = $reservations->orderBy('created_at', 'desc');
        }
        if (!empty($params['per_page'])) {
            $reservations = $reservations->paginate($params['per_page']);
        } else {
            $reservations = $reservations->get();
        }
        return $reservations;
    }

    public function getCalendarReservations($userId = null, $isAdmin = false, $hallId = null)
    {
        $reservations = $this->getReservations(['hall_id' => $hallId]);
        $events = [];
        foreach ($reservations as $reservation) {
            if ($userId == $reservation->user_id || $isAdmin ? true : false) {
                $backgroundColor = '#6ba5c1';
                $borderColor = '#3a87ad';
                $cantEdit = false;
            } else {
                $backgroundColor = '#dc3545';
                $borderColor = '#c73f4c';
                $cantEdit = true;
            }
            $events[] = [
                'id' => $reservation->id,
                'title' => $reservation->name,
                'start' => $reservation->date_from,
                'end' => $reservation->date_to,
                'backgroundColor' => $backgroundColor,
                'borderColor' => $borderColor,
                'cant_edit' => $cantEdit
            ];
        }
        return $events;
    }
}