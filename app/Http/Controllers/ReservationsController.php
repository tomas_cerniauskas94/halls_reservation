<?php

namespace App\Http\Controllers;

use App\Models\Hall;
use App\Models\Reservation;
use App\Services\ReservationsService;
use Illuminate\Http\Request;
use Validator;

class ReservationsController extends Controller
{
    private $rs;

    public function __construct(ReservationsService $rs)
    {
        $this->rs = $rs;
    }

    public function index()
    {
        $halls = Hall::orderBy('name', 'asc')->paginate(50);

        return view('reservations.index', compact('halls'));
    }

    public function show($hallId)
    {
        $hall = Hall::find($hallId);
        if (!$hall) {
            return redirect()->route('reservations.index');
        }

        if(auth()->check()){
            $user = auth()->user();
            $userId = $user->id;
            $isAdmin = $user->is_admin;
        } else {
            $userId = null;
            $isAdmin = false;
        }

        $calendarEvents = $this->rs->getCalendarReservations($userId, $isAdmin, $hall->id);

        return view('reservations.show', compact('hall', 'calendarEvents'));
    }

    public function ajaxCreateReservation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date_from' => 'required|date',
            'date_to' => 'required|date',
            'event_name' => 'required|max:255',
            'hall_id' => 'required|exists:halls,id',
        ]);

        $validator->after(function ($validator) use ($request) {
            $reservations = Reservation::whereBetween('date_from', [$request->date_from, $request->date_to])->whereBetween('date_to', [$request->date_from, $request->date_to])->get();
            if ($reservations->count() > 0) {
                $validator->errors()->add('date_from', 'Šis laikas jau yra rezervuotas.');
            }
        });

        if ($validator->fails()) {
            return response()->json(['success' => false, 'messages' => $validator->errors()]);
        }

        $reservation = Reservation::create(['name' => $request->event_name, 'date_from' => $request->date_from, 'date_to' => $request->date_to, 'user_id' => auth()->id(), 'hall_id' => $request->hall_id]);
        $calendarEvent = [
            'id' => $reservation->id,
            'title' => $reservation->name,
            'start' => $reservation->date_from,
            'end' => $reservation->date_to,
            'backgroundColor' => '#6ba5c1',
            'borderColor' => '#3a87ad',
            'cant_edit' => false
        ];

        return response()->json(['success' => true, 'messages' => 'Rezervacija sėkminga.', 'event' => $calendarEvent]);
    }

    public function ajaxUpdateReservation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_id' => 'required|exists:reservations,id',
            'date_from' => 'required|date',
            'date_to' => 'required|date',
        ]);

        $validator->after(function ($validator) use ($request) {
            $reservations = Reservation::where('id', '!=', $request->event_id)->whereBetween('date_from', [$request->date_from, $request->date_to])->whereBetween('date_to', [$request->date_from, $request->date_to])->get();
            if ($reservations->count() > 0) {
                $validator->errors()->add('date_from', 'Šis laikas jau yra rezervuotas.');
            }
        });

        if ($validator->fails()) {
            return response()->json(['success' => false, 'messages' => $validator->errors()]);
        }

        $user = auth()->user();
        $reservation = $this->rs->getEditableUserReservation($user, $request->event_id);
        $reservation->update(['date_from' => $request->date_from, 'date_to' => $request->date_to]);

        return response()->json(['success' => true, 'messages' => 'Rezervacija atnaujinta.']);
    }

    public function ajaxDeleteReservation(Request $request)
    {
        $user = auth()->user();

        $reservation = Reservation::where('id', $request->event_id);
        if (!$user->is_admin) {
            $reservation = $reservation->where('user_id', $user->id);
        }
        $reservation = $reservation->first();

        if (!$reservation) {
            return response()->json(['success' => false, 'messages' => 'Rezervacija nerasta.']);
        }

        $reservation->delete();

        return response()->json(['success' => true, 'messages' => 'Rezervacija ištrinta.']);
    }
}
