<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PathsHelper;
use App\Http\Requests\StoreHall;
use App\Http\Requests\UpdateHall;
use App\Models\Hall;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class HallsController extends Controller
{
    public function index(Request $request)
    {
        $halls = new Hall;
        if (!empty($request->name)) {
            $halls = $halls->where('name', 'like', '%' . $request->name . '%');
        }
        $halls = $halls->orderBy('name', 'asc')->paginate(10);

        return view('admin.halls.index', compact('halls'));
    }

    public function create()
    {
        return view('admin.halls.create');
    }

    public function store(StoreHall $request)
    {
        $photoName = null;

        if ($request->has('photo')) {
            $photoName = $this->handlePhotoUpload($request->file('photo'));
        }

        Hall::create(['name' => $request->name, 'photo' => $photoName]);

        Session::flash('success', 'Salė sukurta.');
        return redirect()->route('admin.halls.index');
    }

    public function edit($hallId)
    {
        $hall = Hall::find($hallId);
        if (!$hall) {
            Session::flash('error', 'Salė nerasta');
            return redirect()->route('admin.halls.index');
        }
        return view('admin.halls.edit', compact('hall'));
    }

    public function update(UpdateHall $request)
    {
        $hall = Hall::find($request->hall_id);

        $photoName = $hall->photo;

        if ($request->delete_photo) {
            @unlink(public_path(PathsHelper::getHallPhoto($photoName, false)));
            $photoName = null;
        }

        if ($request->has('photo')) {
            $photoName = $this->handlePhotoUpload($request->file('photo'));
        }

        $hall->update(['name' => $request->name, 'photo' => $photoName]);

        Session::flash('success', 'Salė paredaguota.');
        return redirect()->route('admin.halls.index');
    }

    public function delete(Request $request)
    {
        $hall = Hall::find($request->hall_id);
        if ($hall) {
            $hall->delete();
            Session::flash('success', 'Salė ištrinta.');
            return redirect()->route('admin.halls.index');
        }
        Session::flash('error', 'Salė nerasta.');
        return redirect()->route('admin.halls.index');
    }

    private function handlePhotoUpload($uploadedPhoto)
    {
        $hallsImagesFolder = PathsHelper::makeFolderIfDoNotExists(public_path('uploads/halls'));
        $photoName = PathsHelper::uniqueFileName($hallsImagesFolder . '/' . $uploadedPhoto->getClientOriginalName());
        $uploadedPhoto->move($hallsImagesFolder, $photoName);

        return $photoName;
    }
}
