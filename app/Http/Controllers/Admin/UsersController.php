<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Session;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users = User::where('id', '!=', auth()->id());
        if(!empty($request->name)){
            $users = $users->where('name', 'like', '%'.$request->name.'%');
        }
        if(!empty($request->email)){
            $users = $users->where('name', 'like', '%'.$request->email.'%');
        }
        $users = $users->orderBy('name', 'asc')->paginate(10);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(StoreUser $request)
    {
        $validated = $request->validated();
        $validated['password'] = Hash::make($validated['password']);

        User::create($validated);

        Session::flash('success', 'Vartotojas sukurtas.');
        return redirect()->route('admin.users.index');
    }

    public function edit($userId)
    {
        $user = User::find($userId);
        if (!$user) {
            Session::flash('error', 'Vartotojas nerastas');
            return redirect()->route('admin.users.index');
        }
        return view('admin.users.edit', compact('user'));
    }

    public function update(UpdateUser $request)
    {
        $user = User::find($request->user_id);

        if (!empty($request->password)) {
            $password = Hash::make($request->password);
        } else {
            $password = $user->password;
        }

        $user->update(['name' => $request->name, 'email' => $request->email, 'password' => $password]);

        Session::flash('success', 'Vartotojas paredaguotas.');
        return redirect()->route('admin.users.index');
    }

    public function delete(Request $request)
    {
        $user = User::where('id', $request->user_id)->where('id', '!=', auth()->id())->first();
        if ($user) {
            $user->delete();
            Session::flash('success', 'Vartotojas ištrintas.');
            return redirect()->route('admin.users.index');
        }

        Session::flash('error', 'Vartotojas nerastas');
        return redirect()->route('admin.users.index');
    }
}
