<?php

namespace App\Providers;

use App\Models\Hall;
use App\Observers\HallObserver;
use App\Observers\UserObserver;
use App\Services\ReservationsService;
use App\User;
use Illuminate\Support\ServiceProvider;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Hall::observe(HallObserver::class);
        User::observe(UserObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ReservationsService::class, function ($app) {
            return new ReservationsService;
        });
    }
}
