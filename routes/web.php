<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'ReservationsController@index')->name('reservations.index');
Route::get('/halls-reservation/{hallId}', 'ReservationsController@show')->name('reservations.show');

Route::group(['middleware' => ['auth']], function () {
    Route::post('ajax/create-reservation', 'ReservationsController@ajaxCreateReservation')->name('reservations.ajaxCreateReservation');
    Route::post('ajax/update-reservation', 'ReservationsController@ajaxUpdateReservation')->name('reservations.ajaxUpdateReservation');
    Route::post('ajax/delete-reservation', 'ReservationsController@ajaxDeleteReservation')->name('reservations.ajaxDeleteReservation');
});

Route::group(['middleware' => ['admin']], function () {
    //users
    Route::get('/admin/users', 'Admin\UsersController@index')->name('admin.users.index');
    Route::get('/admin/users/create', 'Admin\UsersController@create')->name('admin.users.create');
    Route::post('/admin/users/store', 'Admin\UsersController@store')->name('admin.users.store');
    Route::get('/admin/users/edit/{userId}', 'Admin\UsersController@edit')->name('admin.users.edit');
    Route::post('/admin/users/update', 'Admin\UsersController@update')->name('admin.users.update');
    Route::post('/admin/users/delete', 'Admin\UsersController@delete')->name('admin.users.delete');

    //halls
    Route::get('/admin/halls', 'Admin\HallsController@index')->name('admin.halls.index');
    Route::get('/admin/halls/create', 'Admin\HallsController@create')->name('admin.halls.create');
    Route::post('/admin/halls/store', 'Admin\HallsController@store')->name('admin.halls.store');
    Route::get('/admin/halls/edit/{hallId}', 'Admin\HallsController@edit')->name('admin.halls.edit');
    Route::post('/admin/halls/update', 'Admin\HallsController@update')->name('admin.halls.update');
    Route::post('/admin/halls/delete', 'Admin\HallsController@delete')->name('admin.halls.delete');
});
