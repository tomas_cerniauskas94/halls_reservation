$('[data-type=submit-delete-form]').click(function () {
    if (confirm('Ar tikrai norite ištrinti?')) {
        $($(this).data('form')).submit();
    }
    return false;
});

function addLoading(element, append, uniqueId) {
    var element = $(element);
    var loader = '<div id="' + uniqueId + '" class="loader loader-sm"></div>';
    if (append) {
        element.append(loader);
    } else {
        element.after(loader);
    }
}