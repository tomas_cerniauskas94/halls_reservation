@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Vartotojai</div>

                        <div class="card-body">
                            <form class="form-inline">
                                <div class="form-group">
                                    <input type="text" id="name" name="name" value="{{ request()->input('name') }}" class="form-control mr-2" placeholder="Vardas">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="email" name="email" value="{{ request()->input('email') }}" class="form-control mr-2" placeholder="Elektroninis paštas">
                                </div>
                                <button type="submit" class="btn btn-primary mr-2">Ieškoti</button>
                                <a class="btn btn-secondary" href="{{ route('admin.users.index') }}">Iš naujo</a>
                            </form>
                            @if($users->count() >0)
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Vartotojas</th>
                                                <th>Elektroninis paštas</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                    <td>{{ $user->id }}</td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>
                                                        <a href="{{ route('admin.users.edit', ['userId' => $user->id]) }}"><span class="fa fa-pencil"></span></a>
                                                        <form action="{{ route('admin.users.delete') }}" id="delete-user-form-{{ $user->id }}" class="d-inline-block" method="post">
                                                            @csrf
                                                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                            <a data-type="submit-delete-form" data-form="#delete-user-form-{{ $user->id }}" href="" class="text-danger"><span class="fa fa-trash"></span></a>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {!! $users->links() !!}
                            @else
                                Vartotojų nėra
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection