@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Pridėti vartotoją</div>
                        <div class="card-body">
                            <form action="{{ route('admin.users.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Vartotojo vardas:</label>
                                    <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">Elektroninis paštas:</label>
                                    <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control">
                                    @if($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Slaptažodis:</label>
                                    <input type="password" id="password" name="password" value="{{ old('password') }}" class="form-control">
                                    @if($errors->has('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Pakartokite slaptažodį:</label>
                                    <input type="password" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control">
                                    @if($errors->has('password_confirmation'))
                                        <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-block btn-primary">Sukurti</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
