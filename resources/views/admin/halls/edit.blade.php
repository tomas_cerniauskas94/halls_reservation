@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Salė <strong>{{ $hall->name }}</strong></div>
                        <div class="card-body">
                            <form action="{{ route('admin.halls.update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="hall_id" value="{{ $hall->id }}">
                                <div class="form-group">
                                    <label for="name">Pavadinimas:</label>
                                    <input type="text" id="name" name="name" value="{{ $hall->name }}" placeholder="Pavadinimas" class="form-control">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="photo">Nuotrauka:</label>
                                    @if(!empty($hall->photo))
                                        <div class="mb-1">
                                            <div class="mb-1">
                                                <img src="{{ \App\Helpers\PathsHelper::getHallPhoto($hall->photo) }}" class="thumb">
                                            </div>
                                            <input type="checkbox" name="delete_photo" value="1" {{ old('delete_photo') == 1 ? 'checked':'' }}> Ištrinti nuotrauką
                                        </div>
                                    @endif
                                    <input type="file" id="photo" name="photo">
                                    @if($errors->has('photo'))
                                        <div class="text-danger">{{ $errors->first('photo') }}</div>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-block btn-primary">Redaguoti</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection