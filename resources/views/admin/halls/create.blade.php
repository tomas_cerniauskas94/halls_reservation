@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Pridėti salę</div>
                        <div class="card-body">
                            <form action="{{ route('admin.halls.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Pavadinimas:</label>
                                    <input type="text" id="name" name="name" value="{{ old('name') }}" placeholder="Pavadinimas" class="form-control">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="photo">Nuotrauka:</label>
                                    <input type="file" id="photo" name="photo">
                                    @if($errors->has('photo'))
                                        <div class="text-danger">{{ $errors->first('photo') }}</div>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-block btn-primary">Sukurti</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
