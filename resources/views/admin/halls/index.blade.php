@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Salės</div>

                        <div class="card-body">
                            <form class="form-inline">
                                <div class="form-group">
                                    <input type="text" id="name" name="name" value="{{ request()->input('name') }}" class="form-control mr-2" placeholder="Pavadinimas">
                                </div>
                                <button type="submit" class="btn btn-primary mr-2">Ieškoti</button>
                                <a class="btn btn-secondary" href="{{ route('admin.halls.index') }}">Iš naujo</a>
                            </form>
                            @if($halls->count() >0)
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Pavadinimas</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($halls as $hall)
                                                <tr>
                                                    <td>{{ $hall->id }}</td>
                                                    <td>{{ $hall->name }}</td>
                                                    <td>
                                                        <a href="{{ route('admin.halls.edit', ['hallId' => $hall->id]) }}"><span class="fa fa-pencil"></span></a>
                                                        <form action="{{ route('admin.halls.delete') }}" id="delete-hall-form-{{ $hall->id }}" class="d-inline-block" method="post">
                                                            @csrf
                                                            <input type="hidden" name="hall_id" value="{{ $hall->id }}">
                                                            <a data-type="submit-delete-form" data-form="#delete-hall-form-{{ $hall->id }}" href="" class="text-danger"><span class="fa fa-trash"></span></a>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {!! $halls->links() !!}
                            @else
                                Salių nėra
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection