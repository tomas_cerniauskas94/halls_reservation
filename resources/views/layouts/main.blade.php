<html>
<head>
    <title>Salių rezervacija</title>
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-4.1.2/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel='stylesheet' href="{{ asset('assets/plugins/fullcalendar-3.9.0/fullcalendar.min.css') }}">
    <script>window.laravel = '{{ csrf_token() }}';</script>
    <script src="{{ asset('assets/plugins/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-4.1.2/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/moment.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/fullcalendar-3.9.0/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/fullcalendar-3.9.0/locale/lt.js') }}"></script>
    @yield('top_scripts')
</head>
<body class="bg-light">
    @include('layouts.includes._navbar')
    @if(Session::has('success'))
        <div class="row mt-3">
            <div class="col-md-8 offset-md-2 col-xs-12">
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="row mt-3">
            <div class="col-md-8 offset-md-2 col-xs-12">
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            </div>
        </div>
    @endif
    @yield('content')
    <div class="loading-background">
        <img src="{{ asset('assets/img/loading.gif') }}">
    </div>
    <input type="hidden" id="csrf" name="csrf" value="{{ csrf_token() }}">
    <input type="hidden" id="logged_in" name="logged_in" value="{{ auth()->check() ? 1:0 }}">
    <input type="hidden" id="delete_reservation_url" name="delete_reservation_url" value="{{ route('reservations.ajaxDeleteReservation') }}">
    <input type="hidden" id="update_reservation_url" name="update_reservation_url" value="{{ route('reservations.ajaxUpdateReservation') }}">
    <input type="hidden" id="create_reservation_url" name="create_reservation_url" value="{{ route('reservations.ajaxCreateReservation') }}">
    <script src="{{ asset('assets/js/common.js') }}"></script>
    @yield('bottom_scripts')
</body>
</html>