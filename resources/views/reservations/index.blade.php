@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Salės</div>

                        <div class="card-body">
                            @if(count($halls) > 0)
                            <strong>Pasirinkite salę:</strong>
                            @foreach($halls->chunk(4) as $chunk)
                                <div class="row">
                                    @foreach($chunk as $hall)
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <div class="hall-block d-table">
                                                <div class="d-table-row">
                                                    <div class="d-table-cell">
                                                        <div class="image-wrapper">
                                                            <img class="responsive-img" src="{{ !empty($hall->photo) ? \App\Helpers\PathsHelper::getHallPhoto($hall->photo):asset('assets/img/no-image.jpg') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-table-row">
                                                    <div class="d-table-cell text-center">
                                                        <a class="title" href="{{ route('reservations.show', ['hallId' => $hall->id]) }}">{{ $hall->name }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                            @else
                                Šiuo metu salių nėra.
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection