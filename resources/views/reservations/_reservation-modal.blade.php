<div class="modal fade" id="reservation_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Rezervuoti salę</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="status"></div>
                <p>
                    <strong>Rezervacijos pradžia: <span id="start_date"></span></strong>
                </p>
                <form id="reservation-form" action="" method="post">
                    <div class="form-group">
                        <label>Renginio pavadinimas:</label>
                        <input type="hidden" name="start_date">
                        <input type="text" name="name" placeholder="Renginio pavadinimas" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-block btn-primary">Rezervuoti</button>
                </form>
            </div>
        </div>
    </div>
</div>