@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="card">
                        <div class="card-header">Salė <strong>{{ $hall->name }}</strong></div>

                        <div class="card-body">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="hall_id" id="hall_id" value="{{ $hall->id }}">
        </div>
    </section>
    @include('reservations._reservation-modal')
@endsection

@section('bottom_scripts')
    <script>
        var calendar = {
            createReservationUrl: $('#create_reservation_url').val(),
            updateReservationUrl: $('#update_reservation_url').val(),
            deleteReservationUrl: $('#delete_reservation_url').val(),
            csrf_token: $('#csrf').val(),
            init: function (containerId) {
                var calendar = $(containerId);
                var self = this;

                calendar.fullCalendar({

                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaDay',

                    },
                    // stick:true,
                    defaultView: 'month',
                    defaultTimedEventDuration: '01:00:00',

                    selectable: true,
                    editable: self.checkIfLoggendIn(),
                    //shows time in 1 hour slot
                    slotDuration: '01:00:00',
                    // slotLabelInterval: 15,
                    slotLabelFormat: 'HH:mm',

                    eventConstraint: {
                        start: '00:00',
                        end: '24:00',
                    },

                    events: JSON.parse('<?php echo json_encode($calendarEvents); ?>'),
                    eventAfterRender: function (event, element) {
                        //repeticijos trynimas
                        if (!event.cant_edit) {
                            element.find(".fc-content").prepend("<span class='remove-event-button'><i class='fa fa-close'></i></span>");
                            element.find(".remove-event-button").on('click', function () {
                                self.deleteReservation(calendar, event.id, event._id);
                            });
                        }
                    },
                    select: function (start, end, jsEvent, view) {
                        //jei renkamasis iš mėnesio dienų ar savaičių vaizdo, atidaromas valandų pasirinkimas
                        if (view.name == 'month' || view.name == 'basicWeek') {
                            calendar.fullCalendar('changeView', 'agendaDay');
                            calendar.fullCalendar('gotoDate', start);
                            return false;
                        }
                        if(!self.checkIfLoggendIn()){
                            return false;
                        }
                        //patikrinama ar neužimta data
                        if (self.checkIfTimeIsReserved(calendar, start, end, calendar.fullCalendar('clientEvents'))) {
                            return false;
                        }

                        //patikrinama ar data nepasirinkta senesnė už šiandieną
                        if (start.isAfter(moment())) {

                            var eventTitle = prompt("Provide Event Title");
                            if (eventTitle) {
                                self.createReservation(calendar, start.format("YYYY-MM-DD HH:mm:ss"), end.format("YYYY-MM-DD HH:mm:ss"), eventTitle);
                                // alert('Appointment booked at: ' + start.format("YYYY-MM-DD HH:mm:ss"));
                            }
                            return true;
                        } else {
                            calendar.fullCalendar('unselect');
                            alert('Laikas negali būti mažesnis nei yra dabar.');
                            return false;
                        }
                    },
                    eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                        if (event.cant_edit) {
                            revertFunc();
                            return false;
                        }
                        var start = event.start;
                        var end = event.end;
                        var eventId = event._id;

                        if (self.checkIfTimeIsReserved(calendar, start, end, calendar.fullCalendar('clientEvents'), eventId)) {
                            revertFunc();
                            return false;
                        }
                        self.updateReservation(calendar, event.id, start.format("YYYY-MM-DD HH:mm:ss"), end.format("YYYY-MM-DD HH:mm:ss"));
                    },
                    eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                        if (event.cant_edit) {
                            revertFunc();
                            return false;
                        }
                        var start = event.start;
                        var end = event.end;
                        var eventId = event._id;

                        if (self.checkIfTimeIsReserved(calendar, start, end, calendar.fullCalendar('clientEvents'), eventId)) {
                            revertFunc();
                            return false;
                        }
                        self.updateReservation(calendar, event.id, start.format("YYYY-MM-DD HH:mm:ss"), end.format("YYYY-MM-DD HH:mm:ss"));
                    },

                });
            },
            checkIfTimeIsReserved: function (calendar, start, end, reservations, selectedEventId) {
                //patikrinama ar pasirinktu laiku nėra rezervacijos
                var wrongTime = false;
                if (typeof selectedEventId == 'undefined') {
                    selectedEventId = null;
                }
                $.each(reservations, function (eventIndex, event) {
                    var eStart = new Date(event.start);
                    var eEnd = new Date(event.end);
                    if (((Math.round(start) > Math.round(eStart) && Math.round(start) < Math.round(eEnd))
                        ||
                        (Math.round(end) > Math.round(eStart) && Math.round(end) < Math.round(eEnd))
                        ||
                        (Math.round(start) < Math.round(eStart) && Math.round(end) > Math.round(eEnd))
                        ||
                        Math.round(start) == Math.round(eStart)
                        ||
                        Math.round(end) == Math.round(eEnd)
                        ||
                        event.allDay
                    ) && selectedEventId != event._id) {
                        wrongTime = true;
                        return false;
                    }
                });

                if (wrongTime) {
                    alert('Šis laikas jau yra rezervuotas.');
                    calendar.fullCalendar('unselect');
                    return true;
                }
                return false;
            },
            createReservation: function (container, start, end, name) {
                var self = this;
                var loading = $('.loading-background');
                loading.show();
                $.post(self.createReservationUrl, {
                    date_from: start,
                    date_to: end,
                    event_name: name,
                    _token: self.csrf_token,
                    hall_id: $('#hall_id').val()
                }, function (data) {
                    if (data.success) {
                        alert('Rezervacija priskirta sėkmingai.');
                        var event = data.event;
                        container.fullCalendar('renderEvent', event);
                    } else {
                        alert('Įvyko klaida.');
                    }
                    loading.hide();
                });
            },
            checkIfLoggendIn: function () {
                return $('#logged_in').val() == 0 ? false:true;
            },
            updateReservation: function (container, eventId, start, end) {
                var self = this;
                var loading = $('.loading-background');
                loading.show();
                $.post(self.updateReservationUrl, {
                    date_from: start,
                    date_to: end,
                    event_id: eventId,
                    _token: self.csrf_token,
                }, function (data) {
                    if (data.success) {
                        alert('Rezervacijos laikas pakeistas.');
                    } else {
                        alert('Įvyko klaida.');
                    }
                    loading.hide();
                });
            },
            deleteReservation: function (calendar, eventId, eventSelector) {
                var self = this;
                var loading = $('.loading-background');
                loading.show();
                $.post(self.deleteReservationUrl, {
                    event_id: eventId,
                    _token: self.csrf_token
                }, function (data) {
                    if (data.success) {
                        alert('Rezervacija ištrinta.');
                        calendar.fullCalendar('removeEvents', eventSelector);
                    } else {
                        alert('Įvyko klaida.');
                    }
                    loading.hide();
                });
            }
        };

        calendar.init('#calendar');
    </script>
@endsection